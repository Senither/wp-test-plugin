<?php
/*
	Plugin Name: Test Plugin
	Plugin URI: https://senither.com/
	Description: A simple wordpress plugin that doesn't really do anything.
	Version: 1.0.0
	Author: Senither
	Author URI: https://senither.com
	License: MIT
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/*
|--------------------------------------------------------------------------
| Bootstraps The Plugin
|--------------------------------------------------------------------------
|
| Sets up composers autoloader to load in all of our dependencies, 
| creates our help functions and creates our plugin instance for us.
|
*/

$plugin = require __DIR__ . '/bootstrap.php';

/*
|--------------------------------------------------------------------------
| Sets The Plugin Into Gear
|--------------------------------------------------------------------------
|
| Once every other plugin is loaded we'll boot our own plugin up as well, 
| this is done to ensure that WooCommerce is already loaded when we 
| boot up the plugin, otherwise we'll end up with exceptions and 
| errors being thrown everywhere for missing dependencies.
|
*/

$plugin->onLoad(function () use ($plugin) {
	$plugin->boot();
});
