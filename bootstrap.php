<?php

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our plugin. We just need to utilize it! We'll simply require it into
| the script here so that we don't have to worry about manual loading 
| any of our classes later on. It feels nice to relax.
|
*/

require __DIR__ . '/vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Create a slew of different helper functions to help us with the 
| development process.
|
*/

require __DIR__ . '/helpers.php';

/*
|--------------------------------------------------------------------------
| Creates The Plugin
|--------------------------------------------------------------------------
|
| Creates our plugin instance, and sends it back to the wordpress plugin 
| index file so we can setup our plugin and get to work.
*/

return new \Senither\Wordpress\Test\Plugin;
