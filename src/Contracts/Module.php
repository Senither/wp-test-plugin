<?php

namespace Senither\Wordpress\Test\Contracts;

use Closure;
use Senither\Wordpress\Test\Plugin;
use Senither\Wordpress\Test\Traits\Reference;

abstract class Module
{
	use Reference;

	private $variableDefinition = '{$%s$}';

	public function hasVariable($content, $variable)
	{
		return strpos($content, sprintf($this->variableDefinition, mb_strtolower($variable))) !== false;
	}

	public function replaceVariable($content, $variable, $replacement)
	{
		if (is_callable($replacement)) {
			$replacement = $replacement();
		}

		return str_replace(sprintf($this->variableDefinition, mb_strtolower($variable)), $replacement, $content);
	}

	public function renderView($view, $variables = [])
	{
		return Plugin::getInstance()->renderView($view, $variables);
	}
}