<?php

namespace Senither\Wordpress\Test\Modules;

use Senither\Wordpress\Test\Contracts\Module;

class TestModule extends Module
{
	public function __construct()
	{
		$this->addFilter('the_content', 'content');
	}

	public function content($content)
	{
		return $this->replaceVariable($content, 'test-module', function () use ($content) {
			return '123';
		});

		return $this->renderView('test', compact('content'));
	}
}