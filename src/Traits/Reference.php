<?php 

namespace Senither\Wordpress\Test\Traits;

trait Reference
{
	public function addAction($tag, $method, $priority = 10, $accepted_args = 1)
	{
		add_action($tag, [$this, $method], $priority, $accepted_args);

		return $this;
	}

	public function addFilter($tag, $method)
	{
		add_filter($tag, [$this, $method]);

		return $this;
	}
}