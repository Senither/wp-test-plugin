<?php 

namespace Senither\Wordpress\Test;

use Closure;

class Plugin
{
	static protected $instance = null;

	protected $viewFolder = __DIR__ . '/resources/views/';

	protected $modules = [
		Modules\TestModule::class,
	];

	protected $instances = [
		'modules' => [],
	];

	public function onLoad(Closure $closure)
	{
		add_action('plugins_loaded', $closure);
	}

	public function boot()
	{
		self::$instance = $this;

		$this->registerModules();
	}

	protected function registerModules()
	{
		foreach ($this->modules as $module) {
			$this->instances['modules'][$module] = new $module;
		}
	}

	public function renderView($view, $variables = [])
	{
		$viewPath = $this->viewFolder."{$view}.php";

		if (! file_exists($viewPath)) {
			throw new Exception('View "'.$view.'" was not found!');
		}

		extract($variables);

		return require $viewPath;
	}

	static public function getInstance()
	{
		return self::$instance;
	}
}